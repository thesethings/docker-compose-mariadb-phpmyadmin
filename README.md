# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* docker-compose with MariaDB and PhpMyAdmin

### How to use ###

* Modify variables with your own given credentials, etc

###### Start all container

<code>docker-compose up -d</code>

###### Stop all container

<code>docker-compose down</code>

###### See container logs 
 
<code>docker logs <containerId or containernname>

###### Connect into Container
      
<code>docker exec -it <containerId or containernname> /bin/bash</code>    

#### Links ####
* [docker-compose command-line reference](https://docs.docker.com/compose/reference/)
* [docker command-line reference](https://docs.docker.com/engine/reference/commandline/docker/)

